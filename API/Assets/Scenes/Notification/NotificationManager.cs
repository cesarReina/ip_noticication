﻿using Unity.Notifications.Android;
using UnityEngine;

public class NotificationManager : MonoBehaviour
{
    AndroidNotificationChannel defaultChanel;
    int id;
    void Start()
    {
        /*AndroidNotificationCenter.CancelAllNotifications();*/
        CreateNotifChannel();
        SendNotification();
        CheckNotification();
        CheckNotificationOff();
    }

    void CreateNotifChannel()
    {
        defaultChanel = new AndroidNotificationChannel()
        {
            Id = "channel_id",
            Name = "Default Channel",
            Importance = Importance.High,
            Description = "Generic notifications",
        };
        AndroidNotificationCenter.RegisterNotificationChannel(defaultChanel);
    }
     void SendNotification()
     {
        AndroidNotification notification = new AndroidNotification();
        notification.Title = "First Step";
        notification.Text = "This is your first game";
        notification.SmallIcon = "icon_0";
        notification.FireTime = System.DateTime.Now.AddSeconds(0.1);

        id = AndroidNotificationCenter.SendNotification(notification, defaultChanel.Id);

        print("Notification was create successful - First Step");
     }

    public void CheckNotification()
    {
        if (AndroidNotificationCenter.CheckScheduledNotificationStatus(id) == NotificationStatus.Scheduled)
        {
            AndroidNotification newnotification = new AndroidNotification();
            newnotification.Title = "Second Step";
            newnotification.Text = "This is an example message, purpose educational";
            newnotification.LargeIcon = "icon_1";
            newnotification.FireTime = System.DateTime.Now.AddSeconds(0.1);

            AndroidNotificationCenter.CancelNotification(id);
            id = AndroidNotificationCenter.SendNotification(newnotification, defaultChanel.Id);

            print("Notification was create successful - Second Step");
        }
        /*else if (AndroidNotificationCenter.CheckScheduledNotificationStatus(id) == NotificationStatus.Unknown)
        {
            AndroidNotification secnotification = new AndroidNotification();
            secnotification.Title = "Third Step";
            secnotification.Text = "I dont know what put here";
            secnotification.LargeIcon = "icon_2";
            secnotification.FireTime = System.DateTime.Now.AddSeconds(10);

            id = AndroidNotificationCenter.SendNotification(secnotification, defaultChanel.Id);

            print("Notification was create successful - Third Step");
        }*/
    }

    public void CheckNotificationOff()
    {
        if (AndroidNotificationCenter.CheckScheduledNotificationStatus(id) == NotificationStatus.Scheduled)
        {
            AndroidNotification secnotification = new AndroidNotification();
            secnotification.Title = "Third Step";
            secnotification.Text = "I dont know what put here";
            secnotification.LargeIcon = "icon_2";
            secnotification.FireTime = System.DateTime.Now.AddSeconds(10);

            AndroidNotificationCenter.CancelNotification(id);
            id = AndroidNotificationCenter.SendNotification(secnotification, defaultChanel.Id);

            print("Notification was create successful - Third Step");
        }
    }

}
